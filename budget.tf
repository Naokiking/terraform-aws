resource "aws_budgets_budget" "budgets_EC2" {


  name = "monthly_budget_EC2"
  budget_type = "COST"
  limit_unit = "USD"
  limit_amount = "10"
  time_period_start = "2022-01-15_00:00"
  time_period_end = "2030-01-15_00:00"
  time_unit = "MONTHLY"

  notification {

    comparison_operator = "GREATER_THAN"
    threshold = 100
    threshold_type = "PERCENTAGE"
    notification_type = "FORECASTED"
    subscriber_email_addresses = [ "naokikoyama3@gmail.com" ]
  }
}