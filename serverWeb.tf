resource "aws_instance" "ServerWeb" {

  ami           = "ami-0c6ebbd55ab05f070"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.Public_SN_Terra.id
  security_groups = [aws_security_group.HTTP.id, aws_security_group.SSH.id]
  monitoring = "true"
  associate_public_ip_address = "true"
  key_name = aws_key_pair.EC2_key_home.key_name

  tags = {
    Name = "ServerWeb"
  }
}
