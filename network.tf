resource "aws_vpc" "VPC_Terra"{

    cidr_block = "192.168.0.0/24"
    tags = {
        "Name" = "VPC_Terra"
    }
}

resource "aws_internet_gateway" "IGW_Terra"{
    vpc_id = aws_vpc.VPC_Terra.id
    tags = {
        "Name" = "IGW_Terra"
    }
}

resource "aws_subnet" "Public_SN_Terra" {

    vpc_id = aws_vpc.VPC_Terra.id
    cidr_block = "192.168.0.0/24"
    availability_zone = "eu-west-3a"

    tags = {
      "Name" = "Public_SN_Terra"
    }
}

resource "aws_route_table" "Public_RT_Terra" {

    vpc_id = aws_vpc.VPC_Terra.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.IGW_Terra.id
    }

    tags = {
      "Name" = "Public_RT_Terra"
    }
}

resource "aws_route_table_association" "Public_RT_Terra_Assoc" {

    subnet_id = aws_subnet.Public_SN_Terra.id
    route_table_id = aws_route_table.Public_RT_Terra.id
}

resource "aws_security_group" "HTTP" {

    name = "HTTP"
    vpc_id = aws_vpc.VPC_Terra.id

    egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_security_group" "SSH" {
  name = "SSH"
  vpc_id = aws_vpc.VPC_Terra.id

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}